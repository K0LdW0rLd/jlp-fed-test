const ProductSpecial = ({ price, specialOffer, includedServices }) => {
    return (
        <div style={{margin: "20px"}}>
            <h3 style={{textAlign: "left"}}>
                <h1>£{price}</h1>
                <p style={{color: "red", textTransform:"capitalize"}}>{specialOffer}</p>
                <p style={{color: "green", textTransform:"capitalize"}}>{includedServices}</p>
            </h3>
        </div>
    );
};

export default ProductSpecial;