import { render, screen } from '@testing-library/react'
import Home from '../pages/index'

describe('Home', () => {
  it('renders a heading', () => {
    const textToFind = "Dishwashers"
    render(<Home />);

    const heading = screen.getByText(textToFind);

    expect(heading).toBeInTheDocument()
  })
})