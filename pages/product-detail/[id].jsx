import ProductCarousel from "../../components/product-carousel/product-carousel";
import ProductSpecial from '../../components/product-special/product-special'
import axios from "axios";
import Link from "next/link";
import styles from "./[id].module.scss"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import Divider from '@mui/material/Divider';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import { useState, useEffect } from "react";

export async function getServerSideProps(context) {
  const id = context.params.id;

  const response = await axios(`https://api.johnlewis.com/mobile-apps/api/v1/products/${id}`)
  const data = response.data;

  return {
    props: { data },
  };
}

const ProductDetail = ({ data }) => {
  const [open, setOpen] = useState(false);
  const longForm = data.details.productInformation;
  const shortForm = data.details.productInformation.slice(0, 400).concat("...");
  const [width, setWidth]   = useState(window.innerWidth);
  const updateDimensions = () => {
      setWidth(window.innerWidth);
  }


  const handleReadMore = () => {
    setOpen(!open)
  }

  useEffect(() => {
    window.addEventListener("resize", updateDimensions);
    return () => window.removeEventListener("resize", updateDimensions);
  }, []);

  return (
    <div>
      <div className={styles.header}>
        <Link href={{
                pathname: "/",
              }}>
          <FontAwesomeIcon icon={faChevronLeft} className={styles.icon}/>
        </Link>
        <h1><div dangerouslySetInnerHTML={{ __html: data.title }} /></h1>
        <div />
      </div>
      <Divider />
      <div className={styles.overall}>
        <div className={styles.content}>
          <ProductCarousel image={data.media.images.urls[0]} />
          <div>
            { width <= 768 &&
              <ProductSpecial 
                price={data.price.now} 
                specialOffer={data.displaySpecialOffer} 
                includedServices={data.additionalServices.includedServices}
              />
            }
            <List>
              <ListItem>
                <h3>Product information</h3>
              </ListItem>
              <ListItem>
                <p>Product Code: {data.code}</p>
              </ListItem>
              <ListItem>
                {!open ? <div><div dangerouslySetInnerHTML={{ __html: shortForm }} /></div> : <div><div dangerouslySetInnerHTML={{ __html: longForm }} /></div>}
              </ListItem>
              <Divider />
              <ListItemButton onClick={handleReadMore}>
                <ListItemText primary="Read More" />
                {open ? <ExpandLess /> : <ExpandMore />}
              </ListItemButton>
            </List>
          </div>
          <Divider />
          <div>
            <br/>
            <h3>Product specification</h3>
            <Divider />
            <List>
              {data.details.features[0].attributes.map((item, id) => (
                <ListItem key={id}>
                  <ListItemText >
                    <div><div dangerouslySetInnerHTML={{ __html: item.name }} /></div>
                  </ListItemText>
                  <div><div dangerouslySetInnerHTML={{ __html: item.value }} /></div>
                </ListItem>
              ))}
            </List>
          </div>
        </div>
        <Divider orientation="vertical" flexItem />
        { width > 768 &&
          <ProductSpecial 
            price={data.price.now} 
            specialOffer={data.displaySpecialOffer} 
            includedServices={data.additionalServices.includedServices}
          />
        }
      </div>
    </div>
  );
};

export default ProductDetail;
